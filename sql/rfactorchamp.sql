-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 26-05-2016 a las 03:10:57
-- Versión del servidor: 5.6.30-0ubuntu0.14.04.1
-- Versión de PHP: 5.6.10-1+deb.sury.org~trusty+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `rfactorchamp`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `campeonatos`
--

DROP TABLE IF EXISTS `campeonatos`;
CREATE TABLE IF NOT EXISTS `campeonatos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `administrador` int(10) unsigned NOT NULL,
  `descripcion` text COLLATE utf8_unicode_ci NOT NULL,
  `mods_id` int(10) unsigned NOT NULL,
  `reglamento` text COLLATE utf8_unicode_ci NOT NULL,
  `public` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `campeonatos_nombre_unique` (`nombre`),
  KEY `campeonatos_id_mod_foreign` (`mods_id`),
  KEY `campeonatos_administrador_foreign` (`administrador`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `campeonatos`
--

INSERT INTO `campeonatos` (`id`, `nombre`, `logo`, `administrador`, `descripcion`, `mods_id`, `reglamento`, `public`, `created_at`, `updated_at`) VALUES
(3, 'Clio Cup 2014', 'http://p0.storage.canalblog.com/08/59/1437977/109453576.jpg', 3, 'Descripcion del campeonato', 1, 'Reglamento', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `circuitos`
--

DROP TABLE IF EXISTS `circuitos`;
CREATE TABLE IF NOT EXISTS `circuitos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pais` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `longitud` int(10) unsigned NOT NULL,
  `imagen` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `circuitos_nombre_unique` (`nombre`),
  KEY `circuitos_pais_foreign` (`pais`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Volcado de datos para la tabla `circuitos`
--

INSERT INTO `circuitos` (`id`, `nombre`, `pais`, `longitud`, `imagen`, `descripcion`, `created_at`, `updated_at`) VALUES
(1, 'Circuito Ricardo Tormo', 'ES', 4051, 'http://www.abc.es/Media/201306/04/CIRCUITO%20CHESTE--644x362.jpg', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Circuito de Montmelo', 'ES', 4627, 'http://www.cronodeporte.com/wp-content/uploads/2009/04/montmelo.jpg', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'Circuito de Albacete', 'ES', 3345, 'http://www.quiencorrehoy.com/wp-content/uploads/2013/06/circuito-de-albacete.jpg', 'Circuito ubicado en Albacete', '2016-05-25 18:58:56', '2016-05-25 18:58:56'),
(12, 'Circuito de MonteBlanco', 'ES', 2950, '', 'sin descripcion', '2016-05-25 23:53:55', '2016-05-25 23:53:55');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `escuderias`
--

DROP TABLE IF EXISTS `escuderias`;
CREATE TABLE IF NOT EXISTS `escuderias` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pais` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `escuderias_nombre_unique` (`nombre`),
  KEY `escuderias_pais_foreign` (`pais`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `escuderias`
--

INSERT INTO `escuderias` (`id`, `nombre`, `pais`, `created_at`, `updated_at`) VALUES
(1, 'Ferrari', 'IT', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Campos Racing', 'ES', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Clasificados CCOL14', NULL, '2016-05-25 22:51:52', '2016-05-25 22:51:52'),
(5, 'Clio Cup 2014', NULL, '2016-05-25 23:15:06', '2016-05-25 23:15:06'),
(6, 'ZDBTV', NULL, '2016-05-25 23:15:06', '2016-05-25 23:15:06');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `eventos`
--

DROP TABLE IF EXISTS `eventos`;
CREATE TABLE IF NOT EXISTS `eventos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `campeonatos_id` int(10) unsigned NOT NULL,
  `order` int(10) unsigned NOT NULL,
  `circuitos_id` int(10) unsigned NOT NULL,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagen` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `tipo` enum('tiempo','vueltas') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'vueltas',
  `duracion` int(11) NOT NULL,
  `log` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `completado` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `eventos_id_campeonato_foreign` (`campeonatos_id`),
  KEY `eventos_id_circuito_foreign` (`circuitos_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `eventos`
--

INSERT INTO `eventos` (`id`, `campeonatos_id`, `order`, `circuitos_id`, `nombre`, `descripcion`, `logo`, `imagen`, `fecha`, `hora`, `tipo`, `duracion`, `log`, `completado`, `created_at`, `updated_at`) VALUES
(1, 3, 1, 1, 'Primera del año', 'Carrera en el circuito Ricardo Tormo', '', '', '2016-05-31', '22:00:00', 'vueltas', 25, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 3, 2, 2, 'Segunda fecha en Montmelo', '', '', '', '2016-06-02', '22:00:00', 'vueltas', 12, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 3, 3, 11, 'Carrera Nº3', '', 'http://www.quiencorrehoy.com/wp-content/uploads/2013/06/circuito-de-albacete.jpg', '', '0000-00-00', '18:30:00', 'vueltas', 30, '', 0, '2016-05-25 18:58:56', '2016-05-25 23:08:08'),
(4, 3, 4, 1, 'Carrera 4', '', '', '', '2016-05-31', '22:10:00', 'vueltas', 20, '', 0, '2016-05-25 19:28:15', '2016-05-25 23:14:18'),
(5, 3, 5, 2, 'Carrera 5', 'Un pequeño resumen', '', '', '2016-07-06', '22:00:00', 'vueltas', 25, '', 0, '2016-05-25 19:34:13', '2016-05-25 23:16:41');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marcas`
--

DROP TABLE IF EXISTS `marcas`;
CREATE TABLE IF NOT EXISTS `marcas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `marcas`
--

INSERT INTO `marcas` (`id`, `nombre`, `created_at`, `updated_at`) VALUES
(1, 'Ferrari', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Mercedes Benz', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'McLaren', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Force India', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2016_05_07_053314_paises_y_marcas', 1),
('2016_05_19_094737_usuarios_pilotos', 2),
('2016_05_19_102854_mods_campeonatos', 2),
('2016_05_19_124334_eventos_resultados', 2),
('2016_05_25_055932_create_puntuacion_campeonatos_table', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mods`
--

DROP TABLE IF EXISTS `mods`;
CREATE TABLE IF NOT EXISTS `mods` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `mods_nombre_unique` (`nombre`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `mods`
--

INSERT INTO `mods` (`id`, `nombre`, `descripcion`, `created_at`, `updated_at`) VALUES
(1, 'Clio Cup 2014', 'Mod del año 2014 de la clio Cup', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paises`
--

DROP TABLE IF EXISTS `paises`;
CREATE TABLE IF NOT EXISTS `paises` (
  `pais` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bandera` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`pais`),
  UNIQUE KEY `paises_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `paises`
--

INSERT INTO `paises` (`pais`, `name`, `bandera`, `created_at`, `updated_at`) VALUES
('--', 'Imaginario', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('AR', 'Argentina', 'http://flags.fmcdn.net/data/flags/mini/ar.png', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('DE', 'Alemania', 'http://flags.fmcdn.net/data/flags/mini/de.png', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('ES', 'España', 'http://flags.fmcdn.net/data/flags/mini/es.png', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('IT', 'Italia', 'http://flags.fmcdn.net/data/flags/mini/it.png', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('UK', 'Gran Bretaña', 'http://flags.fmcdn.net/data/flags/mini/gb.png', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pilotos`
--

DROP TABLE IF EXISTS `pilotos`;
CREATE TABLE IF NOT EXISTS `pilotos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `users_id` int(10) unsigned DEFAULT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `pilotos_user_id_foreign` (`users_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=60 ;

--
-- Volcado de datos para la tabla `pilotos`
--

INSERT INTO `pilotos` (`id`, `users_id`, `nombre`, `created_at`, `updated_at`) VALUES
(2, NULL, 'CapitanAmerica', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, NULL, 'Hormi', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, NULL, 'Fluji', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, NULL, 'Ivry', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pilotos_escuderias`
--

DROP TABLE IF EXISTS `pilotos_escuderias`;
CREATE TABLE IF NOT EXISTS `pilotos_escuderias` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dorsal` int(10) unsigned NOT NULL,
  `vehiculos_id` int(10) unsigned NOT NULL,
  `pilotos_id` int(10) unsigned NOT NULL,
  `escuderias_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `pilotos_escuderias_id_vehiculo_foreign` (`vehiculos_id`),
  KEY `pilotos_escuderias_id_piloto_foreign` (`pilotos_id`),
  KEY `pilotos_escuderias_id_escuderia_foreign` (`escuderias_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `pilotos_escuderias`
--

INSERT INTO `pilotos_escuderias` (`id`, `dorsal`, `vehiculos_id`, `pilotos_id`, `escuderias_id`, `created_at`, `updated_at`) VALUES
(1, 22, 1, 2, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 38, 1, 3, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 41, 2, 4, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 26, 2, 5, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puntuacion_campeonatos`
--

DROP TABLE IF EXISTS `puntuacion_campeonatos`;
CREATE TABLE IF NOT EXISTS `puntuacion_campeonatos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `campeonatos_id` int(10) unsigned NOT NULL,
  `posicion` int(10) unsigned NOT NULL,
  `puntos` int(10) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `puntuacion_campeonatos_campeonatos_id_foreign` (`campeonatos_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Volcado de datos para la tabla `puntuacion_campeonatos`
--

INSERT INTO `puntuacion_campeonatos` (`id`, `campeonatos_id`, `posicion`, `puntos`, `created_at`, `updated_at`) VALUES
(1, 3, 1, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 3, 2, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 3, 3, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 3, 4, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 3, 5, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 3, 6, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 3, 7, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 3, 8, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 3, 9, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 3, 10, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resultados`
--

DROP TABLE IF EXISTS `resultados`;
CREATE TABLE IF NOT EXISTS `resultados` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `eventos_id` int(10) unsigned NOT NULL,
  `posicion` int(10) unsigned NOT NULL,
  `pilotos_id` int(10) unsigned NOT NULL,
  `tiempo_total` decimal(15,6) unsigned NOT NULL,
  `total_vueltas` int(10) unsigned NOT NULL,
  `total_boxes` int(10) unsigned NOT NULL,
  `estado` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `resultados_id_evento_foreign` (`eventos_id`),
  KEY `resultados_id_piloto_foreign` (`pilotos_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `usuario` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_type` enum('admin','editor','normal') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'normal',
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pais` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuarios_usuario_unique` (`usuario`),
  UNIQUE KEY `usuarios_email_unique` (`email`),
  KEY `usuarios_pais_foreign` (`pais`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `usuario`, `password`, `user_type`, `email`, `avatar`, `pais`, `created_at`, `updated_at`, `remember_token`) VALUES
(3, 'Claudio', '$2y$10$mC8CezCv.EtbPDiF1qf4sepIkqtr.3NzMQajlDgKROc3JSegdYPPW', 'admin', 'claudio.lonco@gmail.com', '', 'AR', '0000-00-00 00:00:00', '2016-05-25 03:48:51', 'WZsuHm43FFICeD33GquN8Qcf6nPCX03pnGapf8ltsx55Gi4v3RQtAtswPTHp');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `usuario` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_type` enum('admin','editor','normal') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'normal',
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pais` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuarios_usuario_unique` (`usuario`),
  UNIQUE KEY `usuarios_email_unique` (`email`),
  KEY `usuarios_pais_foreign` (`pais`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vehiculos`
--

DROP TABLE IF EXISTS `vehiculos`;
CREATE TABLE IF NOT EXISTS `vehiculos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `marcas_id` int(10) unsigned NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nomnbre_unico` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tipo` enum('camion','camioneta','crossover','deportivo','electrico','formula','gt','monovolumen','prototipo','superdeportivo','turismo','utilitario') COLLATE utf8_unicode_ci NOT NULL,
  `tipo_motor` enum('diesel','electrico','hibrido','gasolina') COLLATE utf8_unicode_ci NOT NULL,
  `cilindrada` int(10) unsigned NOT NULL,
  `traccion` enum('delantera','integral','trasera') COLLATE utf8_unicode_ci NOT NULL,
  `produccion-anios` varchar(13) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `vehiculos_nomnbre_unico_unique` (`nomnbre_unico`),
  KEY `vehiculos_id_marca_foreign` (`marcas_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `vehiculos`
--

INSERT INTO `vehiculos` (`id`, `marcas_id`, `nombre`, `nomnbre_unico`, `tipo`, `tipo_motor`, `cilindrada`, `traccion`, `produccion-anios`, `created_at`, `updated_at`) VALUES
(1, 1, 'Ferrari 2016', 'Ferrari SF16-H', 'formula', 'hibrido', 1600, 'trasera', '012016-122016', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 2, 'F1 W07 Hybrid', 'F1 W07 Hybrid', 'formula', 'hibrido', 1600, 'trasera', '122016', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vehiculos_mods`
--

DROP TABLE IF EXISTS `vehiculos_mods`;
CREATE TABLE IF NOT EXISTS `vehiculos_mods` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mods_id` int(10) unsigned NOT NULL,
  `vehiculos_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `vehiculos_mods_id_mod_foreign` (`mods_id`),
  KEY `vehiculos_mods_id_vehiculo_foreign` (`vehiculos_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `vehiculos_mods`
--

INSERT INTO `vehiculos_mods` (`id`, `mods_id`, `vehiculos_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 1, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vueltas`
--

DROP TABLE IF EXISTS `vueltas`;
CREATE TABLE IF NOT EXISTS `vueltas` (
  `resultados_id` int(10) unsigned NOT NULL,
  `vuelta` int(10) unsigned NOT NULL,
  `tiempo_vuelta` decimal(10,4) unsigned NOT NULL,
  `parcial_1` decimal(10,4) unsigned DEFAULT NULL,
  `parcial_2` decimal(10,4) unsigned DEFAULT NULL,
  `parcial_3` decimal(10,4) unsigned DEFAULT NULL,
  `boxes` tinyint(1) NOT NULL DEFAULT '0',
  `penalizacion` tinyint(1) NOT NULL DEFAULT '0',
  `gasolina` int(10) unsigned NOT NULL,
  `posicion` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`resultados_id`,`vuelta`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `campeonatos`
--
ALTER TABLE `campeonatos`
  ADD CONSTRAINT `campeonatos_administrador_foreign` FOREIGN KEY (`administrador`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `campeonatos_id_mod_foreign` FOREIGN KEY (`mods_id`) REFERENCES `mods` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `circuitos`
--
ALTER TABLE `circuitos`
  ADD CONSTRAINT `circuitos_pais_foreign` FOREIGN KEY (`pais`) REFERENCES `paises` (`pais`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `escuderias`
--
ALTER TABLE `escuderias`
  ADD CONSTRAINT `escuderias_pais_foreign` FOREIGN KEY (`pais`) REFERENCES `paises` (`pais`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `eventos`
--
ALTER TABLE `eventos`
  ADD CONSTRAINT `eventos_id_campeonato_foreign` FOREIGN KEY (`campeonatos_id`) REFERENCES `campeonatos` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `eventos_id_circuito_foreign` FOREIGN KEY (`circuitos_id`) REFERENCES `circuitos` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `pilotos`
--
ALTER TABLE `pilotos`
  ADD CONSTRAINT `pilotos_user_id_foreign` FOREIGN KEY (`users_id`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `pilotos_escuderias`
--
ALTER TABLE `pilotos_escuderias`
  ADD CONSTRAINT `pilotos_escuderias_id_escuderia_foreign` FOREIGN KEY (`escuderias_id`) REFERENCES `escuderias` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pilotos_escuderias_id_piloto_foreign` FOREIGN KEY (`pilotos_id`) REFERENCES `pilotos` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pilotos_escuderias_id_vehiculo_foreign` FOREIGN KEY (`vehiculos_id`) REFERENCES `vehiculos` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `puntuacion_campeonatos`
--
ALTER TABLE `puntuacion_campeonatos`
  ADD CONSTRAINT `puntuacion_campeonatos_campeonatos_id_foreign` FOREIGN KEY (`campeonatos_id`) REFERENCES `campeonatos` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `resultados`
--
ALTER TABLE `resultados`
  ADD CONSTRAINT `resultados_id_evento_foreign` FOREIGN KEY (`eventos_id`) REFERENCES `eventos` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `resultados_id_piloto_foreign` FOREIGN KEY (`pilotos_id`) REFERENCES `pilotos` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_pais_foreign` FOREIGN KEY (`pais`) REFERENCES `paises` (`pais`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `usuarios_pais_foreign` FOREIGN KEY (`pais`) REFERENCES `paises` (`pais`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `vehiculos`
--
ALTER TABLE `vehiculos`
  ADD CONSTRAINT `vehiculos_id_marca_foreign` FOREIGN KEY (`marcas_id`) REFERENCES `marcas` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `vehiculos_mods`
--
ALTER TABLE `vehiculos_mods`
  ADD CONSTRAINT `vehiculos_mods_id_mod_foreign` FOREIGN KEY (`mods_id`) REFERENCES `mods` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `vehiculos_mods_id_vehiculo_foreign` FOREIGN KEY (`vehiculos_id`) REFERENCES `vehiculos` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `vueltas`
--
ALTER TABLE `vueltas`
  ADD CONSTRAINT `vueltas_id_resultado_foreign` FOREIGN KEY (`resultados_id`) REFERENCES `resultados` (`id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
