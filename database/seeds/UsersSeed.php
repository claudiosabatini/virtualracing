<?php

use Illuminate\Database\Seeder;

class UsersSeed extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        //DB::table('users')->delete();

        DB::table('users')->insert([
            'usuario' => 'Claudio',
            'email' => 'claudio.lonco@gmail.com',
            'user_type'=>'admin',
            'password' => bcrypt('demo'),
            'pais'      => 'AR',
        ]);
    
    }

}
