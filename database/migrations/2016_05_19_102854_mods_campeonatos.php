<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModsCampeonatos extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//www.nsmotorsport.es 
		Schema::create('mods', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nombre',150)->unique();
			$table->text('descripcion');
			$table->timestamps();
		});

		Schema::create('vehiculos_mods', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('mods_id')->unsigned();
			$table->foreign('mods_id')->references('id')->on('mods')->onUpdate('cascade');
			$table->integer('vehiculos_id')->unsigned();
			$table->foreign('vehiculos_id')->references('id')->on('vehiculos')->onUpdate('cascade');
			$table->timestamps();
		});

		Schema::create('campeonatos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nombre',100)->unique();
			$table->string('logo');
			$table->integer('administrador')->unsigned();
			$table->foreign('administrador')->references('id')->on('users')->onUpdate('cascade');
			$table->text('descripcion');
			$table->integer('mods_id')->unsigned();
			$table->foreign('mods_id')->references('id')->on('mods')->onUpdate('cascade');
			$table->text('reglamento');
			$table->boolean('public')->default(1);
			$table->timestamps();
		});

		Schema::create('circuitos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nombre')->unique();
			$table->string('pais',2);
			$table->foreign('pais')->references('pais')->on('paises')->onUpdate('cascade');
			$table->integer('longitud')->unsigned();
			$table->string('imagen');
			$table->text('descripcion');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('circuitos');
		Schema::drop('campeonatos');
		Schema::drop('vehiculos_mods');
		Schema::drop('mods');
	}

}
