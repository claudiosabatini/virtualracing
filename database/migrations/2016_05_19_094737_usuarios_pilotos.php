<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsuariosPilotos extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('usuario',50)->unique();
			$table->string('password');
			$table->enum('user_type',['admin','editor','normal'])->default('normal');
			$table->string('email',100)->unique();
			$table->string('avatar');
			$table->string('pais',2);
			$table->foreign('pais')->references('pais')->on('paises')->onUpdate('cascade');
			$table->timestamps();
		});

		Schema::create('pilotos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('users_id')->unsigned();
			$table->foreign('users_id')->references('id')->on('users')->onDelete('cascade')->null();
			$table->string('nombre');
			$table->timestamps();
		});

		Schema::create('escuderias', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nombre')->unique();
			$table->string('pais',2);
			$table->foreign('pais')->references('pais')->on('paises')->onUpdate('cascade');
			$table->timestamps();
		});

		Schema::create('pilotos_escuderias', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('dorsal')->unsigned();
			$table->integer('vehiculos_id')->unsigned();
			$table->foreign('vehiculos_id')->references('id')->on('vehiculos')->onUpdate('cascade');
			$table->integer('pilotos_id')->unsigned();
			$table->foreign('pilotos_id')->references('id')->on('pilotos')->onUpdate('cascade');
			$table->integer('escuderias_id')->unsigned();
			$table->foreign('escuderias_id')->references('id')->on('escuderias')->onUpdate('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('pilotos_escuderias');
		Schema::drop('escuderias');
		Schema::drop('pilotos');
		Schema::drop('usuarios');
	}

}
