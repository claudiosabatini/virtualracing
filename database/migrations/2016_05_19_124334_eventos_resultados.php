<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EventosResultados extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('eventos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('campeonatos_id')->unsigned();
			$table->foreign('campeonatos_id')->references('id')->on('campeonatos')->onUpdate('cascade');
			$table->integer('order')->unsigned();
			$table->integer('circuitos_id')->unsigned();
			$table->foreign('circuitos_id')->references('id')->on('circuitos')->onUpdate('cascade');
			$table->string('nombre',100);
			$table->text('descripcion');
			$table->string('logo');
			$table->string('imagen');
			$table->date('fecha');
			$table->time('hora');
			$table->enum('tipo',['tiempo','vueltas'])->default('vueltas');
			$table->integer('duracion');
			$table->string('log');
			$table->boolean('completado')->default(0);
			$table->timestamps();
		});

		Schema::create('resultados', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('eventos_id')->unsigned();
			$table->foreign('eventos_id')->references('id')->on('eventos')->onUpdate('cascade');
			$table->integer('posicion')->unsigned();
			$table->integer('pilotos_id')->unsigned();
			$table->foreign('pilotos_id')->references('id')->on('pilotos')->onUpdate('cascade');
			$table->decimal('tiempo_total',15,4)->unsigned();
			$table->integer('total_vueltas')->unsigned();
			$table->integer('total_boxes')->unsigned();
			$table->string('estado');
			$table->timestamps();
		});

		Schema::create('vueltas', function(Blueprint $table)
		{
			$table->integer('resultados_id')->unsigned();
			$table->foreign('resultados_id')->references('id')->on('resultados')->onUpdate('cascade');
			$table->integer('vuelta')->unsigned();
			$table->decimal('tiempo_vuelta',15,4)->unsigned();
			$table->decimal('parcial_1',15,4)->unsigned();
			$table->decimal('parcial_2',15,4)->unsigned();
			$table->decimal('parcial_3',15,4)->unsigned();
			$table->boolean('boxes')->default(0);
			$table->boolean('penalizacion')->default(0);
			$table->integer('gasolina')->unsigned();
			$table->integer('posicion')->unsigned();
			$table->timestamps();
			$table->primary(array('resultados_id', 'vuelta'));
		});


	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('vueltas');
		Schema::drop('resultados');
		Schema::drop('eventos');
	}

}
