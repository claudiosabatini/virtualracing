<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PaisesYMarcas extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('paises', function(Blueprint $table)
		{
			$table->string('pais',2)->primary();
			$table->string('name')->unique();
			$table->string('bandera')->unique();
			$table->timestamps();
		});

		Schema::create('marcas', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nombre');
			$table->timestamps();
		});

		Schema::create('vehiculos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('marcas_id')->unsigned();
			$table->foreign('marcas_id')->references('id')->on('marcas')->onDelete('cascade');
			$table->string('nombre');
			$table->string('nomnbre_unico')->unique();
			$table->enum('tipo',[
				'camion',
				'camioneta', //pick-up
				'crossover',
				'deportivo', //coupe
				'electrico',
				'formula',
				'gt',
				'monovolumen',
				'prototipo',
				'superdeportivo',
				'turismo',
				'utilitario'
			]);
			$table->enum('tipo_motor',[
				'diesel',
				'electrico',
				'hibrido',
				'gasolina'
			]);
			$table->integer('cilindrada')->unsigned();
			$table->enum('traccion',[
				'delantera',
				'integral',
				'trasera'
			]);
			$table->string('produccion-anios', 13); //mmYYYY-mmYYYY
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('vehiculos');
		Schema::drop('marcas');
		Schema::drop("paises");

	}

}
