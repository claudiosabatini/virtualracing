<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePuntuacionCampeonatosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('puntuacion_campeonatos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('campeonatos_id')->unsigned();
			$table->foreign('campeonatos_id')->references('id')->on('campeonatos')->onUpdate('cascade');
			$table->integer('posicion')->unsigned();
			$table->integer('puntos')->unsigned()->default(0);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('puntuacion_campeonatos');
	}

}
