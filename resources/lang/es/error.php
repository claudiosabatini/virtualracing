<?php

return [
	"3box"					=> "Box.. Box.. Box....",
	"algo_no_va_bien"		=> "Creemos que algo no va bien!",
	"titulo_error"			=> "Error",
	"titulo_mantenimiento"	=> "Web en mantenimiento",
	"web_en_mantenimiento"	=> "Una breve parada en boxes.<br /> Volveremos pronto!",
];