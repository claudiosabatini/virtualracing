<html>
	<head>
		<link href='http://fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Candal' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Exo:400,300,600,900' rel='stylesheet' type='text/css'>
		<title>{{trans('error.titulo_error')}}</title>


		<style>
			body {
				margin: 0;
				padding: 0;
				width: 100%;
				height: 100%;
				color: #B0BEC5;
				display: table;
				font-weight: 100;
				font-family: 'Candal';
			}

			.container {
				text-align: center;
				display: table-cell;
				vertical-align: middle;
			}
			/*.container {
				position: fixed; 
				top: -50%; 
				left: -50%; 
				width: 200%; 
				height: 200%;
			}
			.container img {
				position: absolute; 
				top: 0; 
				left: 0; 
				right: 0; 
				bottom: 0; 
				margin: auto; 
				min-width: 50%;
				min-height: 50%;
			}*/
			html { 
				background: url(/img/bg-404-1.jpg) no-repeat center center fixed; 
				-webkit-background-size: cover;
				-moz-background-size: cover;
				-o-background-size: cover;
				background-size: cover;
			}

			.content {
				text-align: center;
				display: inline-block;
			}

			.title {
				font-size: 72px;
				margin-bottom: 40px;
				color: white;
			}
		</style>
	</head>
	<body>
		<div class="container">
			
			<div class="content">
				<div class="title">{{trans('error.3box')}}<br /> {{trans('error.algo_no_va_bien')}}</div>
			</div>
		</div>
	</body>
</html>
