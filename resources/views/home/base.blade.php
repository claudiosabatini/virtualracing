@extends('base')

@section('cover')
	<div class="cover">
		<div class="background-image-fixed cover-image" style="background-image : url('/img/bg-1.jpg')"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<h1 class="text-inverse">Controla tus campeonatos</h1>
					<p class="text-inverse">De una forma rápida y ágil.!!!</p>
					<br><br>
					<a class="btn btn-lg btn-primary">Gestiona tu campeonato ahora!</a>
				</div>
			</div>
		</div>
	</div>


@endsection


@section('content')
	<div class="section">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<h2>Circuitos</h2>
					<p>Una base de datos colaborativa con todos los circuitos. De cualquier país del mundo, y como no, circuitos inventados para las competiciones online.</p>
					<img src="https://carloscastella.files.wordpress.com/2010/06/04-bristol5.gif" class="img-responsive">
					
				</div>
				<div class="col-md-3">
					<img src="/img/17452691-3d-persona-blanca-piloto-de-carreras-con-un-casco-de-carreras-Imagen-en-3D-Aislado-fondo-blanco--Foto-de-archivo.jpg" class="img-responsive">
					<h2>Pilotos</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisici elit,
						<br>sed eiusmod tempor incidunt ut labore et dolore magna aliqua.
						<br>Ut enim ad minim veniam, quis nostrud</p>
				</div>
				<div class="col-md-3">
					<h2>Vehiculos</h2>
					<p>Todos los vehiculos que tengan un mod en un mismo sitio.</p>
					<img src="http://www.autopista.es/media/cache/original/upload/images/article/109752/article-25-coches-mas-bonitos-historia-fotos-5710c2b9d0483.jpg" class="img-responsive img-rounded">
					
				</div>
				<div class="col-md-3">
					<img src="/img/maxresdefault.jpg" class="img-responsive">
					<h2>A disfrutar</h2>
					<p>Y poder ocupar la mayor parte de tu tiempo en pilotar y no en tener que calcular las posiciones y los puntos obtenidos en el campeonato.</p>
				</div>
			</div>
		</div>
	</div>
	<div class="section">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
				<img src="http://pingendo.github.io/pingendo-bootstrap/assets/placeholder.png" class="img-responsive">
				</div>
				<div class="col-md-6">
				<h1 class="text-primary">A title</h1>
				<h3>A subtitle</h3>
				<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo
					ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis
					dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies
					nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.
					Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In
					enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum
					felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus
					elementum semper nisi.</p>
				</div>
			</div>
		</div>
	</div>
	<div class="section">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<h1 class="text-primary">A title</h1>
					<h3>A subtitle</h3>
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo
						ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis
						dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies
						nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.
						Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In
						enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum
						felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus
						elementum semper nisi.</p>
				</div>
				<div class="col-md-6">
					<img src="http://pingendo.github.io/pingendo-bootstrap/assets/placeholder.png" class="img-responsive">
				</div>
			</div>
		</div>
	</div>
	<div class="section">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1 class="text-center text-primary">Desarrollado</h1>
					<p class="text-center">por</p>
				</div>
			</div>
			<div class="row">
				
				<div class="col-md-4 col-md-push-4">
					<img src="http://pingendo.github.io/pingendo-bootstrap/assets/user_placeholder.png" class="center-block img-circle img-responsive">
					<h3 class="text-center">Claudio Sabatini</h3>
					<p class="text-center">Developer</p>
				</div>
				
			</div>
		</div>
	</div>
@endsection