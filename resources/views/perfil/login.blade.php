<html>
	<head>
		<link href='http://fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Candal' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Exo:400,300,600,900' rel='stylesheet' type='text/css'>
		<link href="http://pingendo.github.io/pingendo-bootstrap/themes/default/bootstrap.css" rel="stylesheet" type="text/css">
		<link href="/css/base.css" rel="stylesheet" type="text/css">
		<title>{{trans('general.login')}}</title>


		<style>
			body {
				margin: 0;
				padding: 0;
				width: 100%;
				height: 100%;
				color: #B0BEC5;
				display: table;
				font-family: 'Exo';
			}

			.container {
				text-align: center;
				display: table-cell;
				vertical-align: middle;
			}
			/*.container {
				position: fixed; 
				top: -50%; 
				left: -50%; 
				width: 200%; 
				height: 200%;
			}
			.container img {
				position: absolute; 
				top: 0; 
				left: 0; 
				right: 0; 
				bottom: 0; 
				margin: auto; 
				min-width: 50%;
				min-height: 50%;
			}*/
			html { 
				background: url(/img/bg-user.jpg) no-repeat center center fixed; 
				-webkit-background-size: cover;
				-moz-background-size: cover;
				-o-background-size: cover;
				background-size: cover;
			}

			.content {
				text-align: center;
				display: inline-block;
			}

			.title {
				font-size: 72px;
				margin-bottom: 40px;
				color: white;
			}
		</style>
	</head>
	<body>
		<div class="container">
			@if (count($errors) > 0)
				<div class="col-xs-6 col-xs-push-3 col-xs-3-offset">
					<div class="alert alert-danger">
						<strong>Whoops!</strong> There were some problems with your input.<br><br>
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				</div>
			@endif
			<div class="col-xs-12">
			<div class="content bg-black60">
				<div class="col-xs-7">
					<form class="form-horizontal form-signin" role="form" method="POST" action="/auth/login">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<h2 class="form-signin-heading">Please sign in</h2>
						<label for="inputEmail" class="sr-only">Email address</label>
						<input type="email" id="inputEmail" class="form-control" placeholder="Email address" name="email" required="" autofocus="">
						<label for="inputPassword" class="sr-only">Password</label>
						<input type="password" id="inputPassword" class="form-control" placeholder="Password" name="password" required="">
						<div class="checkbox">
						{{--
							<label>
								<input type="checkbox" value="remember"> Remember me
							</label>
							--}}
						</div>
						<button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>


						{{-- <a href="/password/email">Forgot Your Password?</a> --}}
					</form>
				</div>
				<div class="col-xs-1"><img src="/img/piloto.png"></div>
			</div>
			</div>
		</div>
	</body>
</html>
