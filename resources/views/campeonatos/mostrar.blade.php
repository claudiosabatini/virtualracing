@extends('base')

@section('content')
	<div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="page-header">
              <h1>{{$campeonato->nombre}}
                <small>{{$campeonato->descripcion}}</small>
              </h1>
            </div>
          </div>
        </div>
        
    </div>
	<div class="section">
	  <div class="container">
		<div class="row">
		  <div class="col-md-4">
			<ul class="media-list">
				@foreach ($campeonato->eventos as $evento)
				  <li class="media detalle">
					<a class="pull-left" href="#">
						@if($evento->imagen!="")
							<img class="media-object" src="{{$evento->imagen}}" height="64" width="64">
						@elseif($evento->logo!="")
							<img class="media-object" src="{{$evento->logo}}" height="64" width="64">
						@elseif($evento->circuito->imagen!="")
							<img class="media-object" src="{{$evento->circuito->imagen}}" height="64" width="64">
						@else
							<img class="media-object" src="http://pingendo.github.io/pingendo-bootstrap/assets/placeholder.png" height="64" width="64">
						@endif
					</a>
					<div class="media-body">

					  <h4 class="media-heading">{{$evento->nombre}}</h4>
					  <small>{{$evento->descripcion}}</small>
					  <p><img src="{{$evento->circuito->localizacion->bandera}}" width="20" /> <a href='{{ action('CircuitosController@show',['id'=>$evento->circuito->id,'nombre'=>str_slug($evento->circuito->nombre,'-') ]) }}'>{{$evento->circuito->nombre }}</a></p>
					  @if($evento->completado)
					  	<p><a href="{{ action('EventosController@show',['c_id'=>$campeonato->id,'nombre'=>str_slug($campeonato->nombre,'-'),'id'=>$evento->order ]) }}">Ver resultados</a></p>
					  @elseif(!\Auth::guest() && \Auth::user()->id== $campeonato->administrador) 
						<p><a href="{{ action('EventosController@subir',['c_id'=>$campeonato->id,'nombre'=>str_slug($campeonato->nombre,'-'),'id'=>$evento->order ]) }}">Editar resultados</a></p>
					  @endif
					</div>
				  </li>
			  	@endforeach

			  	@if(!\Auth::guest() && \Auth::user()->id== $campeonato->administrador) 
			  		<li class="media detalle">
					<a class="pull-left" href="#">
						<img class="media-object" src="http://pingendo.github.io/pingendo-bootstrap/assets/placeholder.png" height="64" width="64">
					</a>
					<div class="media-body">

					  <h4 class="media-heading">Agrega un evento</h4>
					  <small></small>
					  
					  	<p><a href="{{ action('EventosController@add',['c_id'=>$campeonato->id,'nombre'=>str_slug($campeonato->nombre,'-')] ) }}">Agregar</a></p>
					</div>
				  </li>
			  	@endif
			</ul>
		  </div>
		  <div class="col-md-1"></div>
		  <div class="col-md-7">
			<table class="table">
			  <thead>
				<tr>
				  <th>#</th>
				  <th>Piloto</th>
				  <th>Escuderia</th>
				  <th>Puntos</th>
				</tr>
			  </thead>
			  {{--dd($campeonato->devolverPosicionesCampeonato())--}}
				{{--dd(($campeonato->posiciones_campeonato))--}}
			  <tbody>
			  	@if(count($campeonato->posiciones_campeonato)>0)
			  		@foreach($campeonato->posiciones_campeonato as $posicion)
						<tr>
						  <td>{{++$posiciones}}</td>
						  <td>{{$posicion['nombre']}}</td>
						  <td>{{$posicion['escuderia']}}</td>
						  <td>{{$posicion['puntos']}}</td>
						</tr>
					@endforeach
				@endif
				
			  </tbody>
			</table>
		  </div>
		</div>
	  </div>
	</div>

@endsection