<html><head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="csrf-token" content="{{ csrf_token()}}">
            <title>rFactorChamps @yield('page_title')</title>
	<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    {{-- <script src="/js/jquery.min.js"></script>
    <script src="/js/jquery-ui.min.js"></script>--}}
	<script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Candal' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Exo:400,300,600,900' rel='stylesheet' type='text/css'>
	<link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="http://pingendo.github.io/pingendo-bootstrap/themes/default/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="/css/base.css" rel="stylesheet" type="text/css">
	<link href="/css/jquery-ui/jquery-ui.css" rel="stylesheet" type="text/css">

	@yield('header')

	</head>
	<body>

		<div class="navbar">
			<div class="container">
				<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/"><span><img src="/img/red.png" width="50"/>Champs</span></a>
				</div>
				<div class="collapse navbar-collapse" id="navbar-ex-collapse">
					<ul class="nav navbar-nav navbar-right">
						<li class="active">
							<a href="/">Home</a>
						</li>
						<li>
							<a href="/campeonatos">Campeonatos</a>
						</li>
						<li>
							<a href="/circuitos">Circuitos</a>
						</li>
						
						@if(\Auth::user()==null)
							<li>
								<a href="/perfil/login">Login</a>
							</li>
						@else
							<li>
								<a href="/pilotos">Pilotos</a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Administrar<i class="fa fa-caret-down"></i></a>
								<ul class="dropdown-menu" role="menu">
									<li>
										<a href="/mis-campeonatos">Campeonatos</a>
									</li>
									<li>
										<a href="/circuitos/agregar">Circuitos</a>
									</li>
									<li>
										<a href="/mis-resultados">Resultados</a>
									</li>
								</ul>
							</li>
							<li>
								<a href="/auth/logout">Cerrar Sesion</a>
							</li>
						@endif
						{{--
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Dropdown <i class="fa fa-caret-down"></i></a>
							<ul class="dropdown-menu" role="menu">
							<li>
								<a href="#">Action</a>
							</li>
							<li>
								<a href="#">Another action</a>
							</li>
							<li>
								<a href="#">Something else here</a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="#">Separated link</a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="#">One more separated link</a>
							</li>
							</ul>
						</li>
						--}}

						{{-- si da tiempo pongo los idiomas
						<li id="control-language-container" class="control_language">
                                <div class="control_language">
                                 <select name="language_option" id="language-selector">                                  
                                    <option @if(\Session::get('applocale')=="en") selected @endif value="en" class="flag-english">en</option>
                                    <option @if(\Session::get('applocale')=="es") selected @endif value="es" class="flag-spanish">es</option>
                                   
                                </select>
                                <a id="language-ticker" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> </a> 
                                <ul id="language-display" class="dropdown-menu" aria-labelledby="language-display"> 
                                   <li data-type="en"><img src="{{ asset('/img/flags/language_flags/flag_GB.png')}}"></li> 
                                   <li data-type="es"><img src="{{ asset('/img/flags/language_flags/flag_ES.png')}}"></li>

                                </ul>

                                </div>
                                
                            </li> --}}
					</ul>
				</div>
			</div>
		</div>
	@yield('cover')
	@yield('content')
	<footer class="section section-primary">
		<div class="container">
		<div class="row">
			<div class="col-sm-6">
			<h1>CEED CV</h1>
			<p>Desarrollado para el proyecto fin de carrera del Modulo de Desarrollo
				de Aplicaciones Web - 2015/2016 - Claudio Sabatini</p>
			</div>
			<div class="col-sm-6">
			<p class="text-info text-right">
				<br>
				<br>
			</p>
			<div class="row">
				<div class="col-md-12 hidden-lg hidden-md hidden-sm text-left">
				<a href="https://www.instagram.com/explore/tags/rfactor/"><i class="fa fa-3x fa-fw fa-instagram text-inverse"></i></a>
				<a href="https://twitter.com/search?q=rfactor&src=spxr"><i class="fa fa-3x fa-fw fa-twitter text-inverse"></i></a>
				<a href="#"><i class="fa fa-3x fa-fw fa-facebook text-inverse"></i></a>
				<a href="#"><i class="fa fa-3x fa-fw fa-github text-inverse"></i></a>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 hidden-xs text-right">
				<a href="https://www.instagram.com/explore/tags/rfactor/"><i class="fa fa-3x fa-fw fa-instagram text-inverse"></i></a>
				<a href="https://twitter.com/search?q=rfactor&src=spxr"><i class="fa fa-3x fa-fw fa-twitter text-inverse"></i></a>
				<a href="#"><i class="fa fa-3x fa-fw fa-facebook text-inverse"></i></a>
				<a href="#"><i class="fa fa-3x fa-fw fa-github text-inverse"></i></a>
				</div>
			</div>
			</div>
		</div>
		</div>
	</footer>
	
@yield('scripts')
</body>
</html>