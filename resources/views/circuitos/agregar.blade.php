@extends('base')

@section('header')
	<link href='/js/chosen/chosen.css' rel='stylesheet' type='text/css'>
	<script type="text/javascript" src="/js/chosen/chosen.jquery.min.js"></script>
	
	<script type="text/javascript" src="/js/jquery.validate.js"></script>

@endsection

@section('scripts')
<script>
	jQuery(".select-circuito").chosen({no_results_text: "Oops, no se ha encontrado nada!",allow_single_deselect: true}); 
	 

	jQuery( "#eventoFecha" ).datepicker({altFormat: "yy-mm-dd",dateFormat: "yy-mm-dd"});

	function agregarCircuito(){
		jQuery('#id-circuito').removeClass('required');
		jQuery('.crear-circuito').removeClass('hidden');

		jQuery('#circuitoNombre').addClass('required');
		jQuery('#paisCircuito').addClass('required');
		jQuery(".select-pais-circuito").chosen({no_results_text: "Oops, el pais no se encuentra!",allow_single_deselect: true});

	}
	jQuery(document).ready(function(){ $("#formEventos").validate(); }); 
</script>
@endsection

@section('content')

<div class="section">
	  <div class="container">
		<div class="row">
		  <div class="col-md-12">
			<div class="page-header">
			  <h1>Agregar Circuito </h1>
			</div>
		  </div>
		</div>
	  </div>
	</div>
	<div class="section">
		<form role="form" method="POST" id="formEventos" action="{{ action('CircuitosController@store')}}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
	  <div class="container">
		<div class="row">
		  <div class="col-md-12">
			<div class="col-md-7">

				

				<div class="crear-circuito">
				<br /><br />
					<div class="form-group">
						<label class="control-label" for="circuitoNombre">Nombre del circuito</label>
						<input class="form-control required" id="circuitoNombre" name="circuitoNombre" placeholder="Nombre del circuito" type="text">
					</div>
					<div class="form-group">
						<label class="control-label" for="paisCircuito">Pais del circuito</label>
						<select data-placeholder="Selecciona el pais" name="paisCircuito" id='paisCircuito' class="form-control select-pais-circuito required" >
							<option></option>
							@foreach($paises as $pais)
								<option value="{{$pais->pais}}">{{$pais->name}}</option>
							@endforeach
						</select>
					</div>

					<div class="form-group">
						<label class="control-label" for="distanciaCircuito">Distancia/Longitud del circuito</label>
						<input class="form-control required" id="distanciaCircuito" name="distanciaCircuito" placeholder="metros" type="text">
					</div>
					<div class="form-group">
						<label class="control-label" for="urlImagenCircuito">URL a la imagen del circuito</label>
						<input class="form-control" id="urlImagenCircuito" name="urlImagenCircuito" placeholder="http://" type="text">
					</div>
					<div class="form-group">
						<label class="control-label" for="descripcionCircuito">Descripcion del circuito</label>
						<textarea class="form-control" name="descripcionCircuito" rows=""></textarea>
					</div>
				</div>

				<button class="btn btn-lg btn-primary btn-block" type="submit">Grabar</button>
			</div>
			
				

			</div>
		  </div>
		</div>
	  </div>
	  </form>
	</div>

@endsection