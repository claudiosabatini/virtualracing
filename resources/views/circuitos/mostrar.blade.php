@extends('base')

@section('cover')
	{{--<div class="cover">
		<div class="background-image-fixed cover-image" style="background-image : url('{{$circuito->imagen}}')"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
				
					<img src="{{$circuito->localizacion->bandera}}" class="img-responsive">
					<h1 class="text-inverse">{{$circuito->nombre}}</h1>
					
				</div>
			</div>
		</div>
	</div>--}}
	<div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="col-md-6 col-md-push-3">
            	<h1 class="text">{{$circuito->nombre}}</h1>
              <img src="{{$circuito->imagen}}" class="img-responsive">
            </div>
          </div>
        </div>
      </div>
    </div>


@endsection


@section('content')

	<div class="section">
		<div class="container">
			<div class="row">
				{{$circuito->descripcion}}
				<hr />
			</div>
		</div>
	</div>
	<div class="section">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<h2>Circuitos</h2>
					<p>Una base de datos colaborativa con todos los circuitos. De cualquier país del mundo, y como no, circuitos inventados para las competiciones online.</p>
					<img src="https://carloscastella.files.wordpress.com/2010/06/04-bristol5.gif" class="img-responsive">
					
				</div>
				<div class="col-md-3">
					<img src="/img/17452691-3d-persona-blanca-piloto-de-carreras-con-un-casco-de-carreras-Imagen-en-3D-Aislado-fondo-blanco--Foto-de-archivo.jpg" class="img-responsive">
					<h2>Pilotos</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisici elit,
						<br>sed eiusmod tempor incidunt ut labore et dolore magna aliqua.
						<br>Ut enim ad minim veniam, quis nostrud</p>
				</div>
				<div class="col-md-3">
					<h2>Vehiculos</h2>
					<p>Todos los vehiculos que tengan un mod en un mismo sitio.</p>
					<img src="http://www.autopista.es/media/cache/original/upload/images/article/109752/article-25-coches-mas-bonitos-historia-fotos-5710c2b9d0483.jpg" class="img-responsive img-rounded">
					
				</div>
				<div class="col-md-3">
					<img src="/img/maxresdefault.jpg" class="img-responsive">
					<h2>A disfrutar</h2>
					<p>Y poder ocupar la mayor parte de tu tiempo en pilotar y no en tener que calcular las posiciones y los puntos obtenidos en el campeonato.</p>
				</div>
			</div>
		</div>
	</div>
	
@endsection