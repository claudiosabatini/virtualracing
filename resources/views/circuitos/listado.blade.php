@extends('base')


@section('content')
	@if(count($circuitos)>0)
		<div class="section">
		  <div class="container">
			<div class="row">
				@for($i=0;$i<count($circuitos);$i++)
					<div class="col-md-4">
						@if($circuitos[$i]->imagen != "")
							<img src="{{$circuitos[$i]->imagen}}"	class="img-responsive">
						@else
							<img src="http://pingendo.github.io/pingendo-bootstrap/assets/placeholder.png"	class="img-responsive">
						@endif
						<h2><a href="{{ action('CircuitosController@show',['id'=>$circuitos[$i]->id,'nombre'=>str_slug($circuitos[$i]->nombre,'-') ]) }}">{{$circuitos[$i]->nombre}} </a></h2>
						{{--<p>{!! $circuitos[$i]->descripcion !!}</p>--}}
					</div>
					@if(($i+1)%3==0 && $i!=1)  
						{{-- cerramos la linea cada 3--}}
						</div><div class="row">
						
					@endif
			  	@endfor
			</div>
		  </div>
		</div>
	@else 
		<div class="cover">
		<div class="background-image-fixed cover-image" style="background-image : url('/img/circuitos_desaparecidos_01.jpg')"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<h1 class="text-inverse">Parece que no hay nada por aqui.</h1>
				</div>
			</div>
		</div>
	</div>
	@endif
@endsection