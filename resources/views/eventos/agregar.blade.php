@extends('base')

@section('header')
	<link href='/js/chosen/chosen.css' rel='stylesheet' type='text/css'>
	<script type="text/javascript" src="/js/chosen/chosen.jquery.min.js"></script>
	
	<script type="text/javascript" src="/js/jquery.validate.js"></script>

@endsection

@section('scripts')
<script>
	jQuery(".select-circuito").chosen({no_results_text: "Oops, no se ha encontrado nada!",allow_single_deselect: true}); 
	 

	jQuery( "#eventoFecha" ).datepicker({altFormat: "yy-mm-dd",dateFormat: "yy-mm-dd"});

	function agregarCircuito(){
		jQuery('#id-circuito').removeClass('required');
		jQuery('.crear-circuito').removeClass('hidden');

		jQuery('#circuitoNombre').addClass('required');
		jQuery('#paisCircuito').addClass('required');
		jQuery(".select-pais-circuito").chosen({no_results_text: "Oops, el pais no se encuentra!",allow_single_deselect: true});

	}
	jQuery(document).ready(function(){ $("#formEventos").validate(); }); 
</script>
@endsection

@section('content')

<div class="section">
	  <div class="container">
		<div class="row">
		  <div class="col-md-12">
			<div class="page-header">
			  <h1>Agregar Evento </h1>
			</div>
		  </div>
		</div>
	  </div>
	</div>
	<div class="section">
		<form role="form" method="POST" id="formEventos" action="{{ action('EventosController@store')}}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="campeonato" value="{{ $campeonato }}">
	  <div class="container">
		<div class="row">
		  <div class="col-md-12">
			<div class="col-md-4">

				<div class="form-group">
				  <label class="control-label" for="circuito">Circuito</label><br />
				  <select data-placeholder="Selecciona el circuito" name="circuito" id="id-circuito" class="form-control select-circuito required" >
						<option></option>
						@foreach($circuitos as $circuito)
							<option value="{{$circuito->id}}">{{$circuito->nombre}}</option>
						@endforeach
				  </select>
				  
				</div>
				<a class="btn btn-primary" id="btn-agregar-circuito" href="javascript:agregarCircuito();">Agregar Circuito</a>
				<!--<a class="btn btn-primary hidden" id="btn-cerrar-agregar-circuito" href="javascript:agregarCircuito();">Seleccionar</a>-->

				<div class="crear-circuito hidden">
				<br /><br />
					<div class="form-group">
						<label class="control-label" for="circuitoNombre">Nombre del circuito</label>
						<input class="form-control" id="circuitoNombre" name="circuitoNombre" placeholder="Nombre del circuito" type="text">
					</div>
					<div class="form-group">
						<label class="control-label" for="paisCircuito">Pais del circuito</label>
						<select data-placeholder="Selecciona el pais" name="paisCircuito" id='paisCircuito' class="form-control select-pais-circuito" >
							<option></option>
							@foreach($paises as $pais)
								<option value="{{$pais->pais}}">{{$pais->name}}</option>
							@endforeach
						</select>
					</div>

					<div class="form-group">
						<label class="control-label" for="distanciaCircuito">Distancia/Longitud del circuito</label>
						<input class="form-control" id="distanciaCircuito" name="distanciaCircuito" placeholder="metros" type="text">
					</div>
					<div class="form-group">
						<label class="control-label" for="urlImagenCircuito">URL a la imagen del circuito</label>
						<input class="form-control" id="urlImagenCircuito" name="urlImagenCircuito" placeholder="http://" type="text">
					</div>
					<div class="form-group">
						<label class="control-label" for="descripcionCircuito">Descripcion del circuito</label>
						<textarea class="form-control" name="descripcionCircuito" rows=""></textarea>
					</div>
				</div>


			</div>
			<div class="col-md-1"></div>
			<div class="col-md-7">
				<div class="form-group">
				  <label class="control-label" for="eventoNombre">Nombre*</label>
				  <input class="form-control required" id="eventoNombre" name="eventoNombre" placeholder="Nombre del evento" type="text">
				</div>
				<div class="form-group">
				  <label class="control-label" for="eventoDescripcion">Descripcion</label>
				  <input class="form-control" id="eventoDescripcion" name="eventoDescripcion" placeholder="Descripcion" type="text">
				</div>
				<div class="form-group">
				  <label class="control-label" for="eventoLogo">Logo image</label>
				  <input class="form-control" id="eventoLogo" name="eventoLogo" placeholder="http://" type="text">
				</div>
				<div class="form-group">
				  <label class="control-label" for="eventoNombre">Fecha y Hora*</label>
				  <div class="row">
						<div class="col-xs-6">
							<input class="form-control required" id="eventoFecha" name="eventoFecha" placeholder="{{date('Y-m-d')}}" type="text">
						</div>
						<div class="col-xs-6">
							<div class="row">
								<div class="col-xs-6">
									<select data-placeholder="Horas" name="eventoHoras" id='eventoHoras' class="form-control select-evento-horas required" >
										<option value="">Horas</option>
										@for($i=0;$i<24;$i++)
											<option value="{{$i}}">{{sprintf('%02d',$i)}}</option>
										@endfor
									</select>
								</div>
								<div class="col-xs-6">
									<select data-placeholder="Minutos" name="eventoMinutos" id='eventoMinutos' class="form-control select-evento-minutos required" >
										<option value="">Minutos</option>
										@for($i=0;$i<60;$i+=5)
											<option value="{{$i}}">{{sprintf('%02d',$i)}}</option>
										@endfor
									</select>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label" for="eventoTipo">Duracion *</label>
					<div class="row">
						<div class="col-xs-6">
							<select data-placeholder="Tipo de ducacion" name="eventoTipo" id='eventoTipo' class="form-control select-pais-duracion" >
								<option value="vueltas">Vueltas</option>
								<option value="tiempo">Tiempo</option>
							</select>
						</div>
						<div class="col-xs-6">
							<input class="form-control required" id="eventoDuracion" name="eventoDuracion" placeholder="Cantidad" type="number">
						</div>
					</div>
				</div>
				<button class="btn btn-lg btn-primary btn-block" type="submit">Grabar</button>

			</div>
		  </div>
		</div>
	  </div>
	  </form>
	</div>

@endsection