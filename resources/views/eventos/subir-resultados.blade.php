@extends('base')

@section('header')
	<link href='/js/chosen/chosen.css' rel='stylesheet' type='text/css'>
	<script type="text/javascript" src="/js/chosen/chosen.jquery.min.js"></script>
	
	<script type="text/javascript" src="/js/jquery.validate.js"></script>

@endsection

@section('scripts')
<script>
	jQuery(".select-circuito").chosen({no_results_text: "Oops, no se ha encontrado nada!",allow_single_deselect: true}); 
	 

	jQuery( "#eventoFecha" ).datepicker({altFormat: "yy-mm-dd",dateFormat: "yy-mm-dd"});

	function agregarCircuito(){
		jQuery('#id-circuito').removeClass('required');
		jQuery('.crear-circuito').removeClass('hidden');

		jQuery('#circuitoNombre').addClass('required');
		jQuery('#paisCircuito').addClass('required');
		jQuery(".select-pais-circuito").chosen({no_results_text: "Oops, el pais no se encuentra!",allow_single_deselect: true});

	}
	jQuery(document).ready(function(){ $("#formEventos").validate(); }); 
</script>
@endsection

@section('content')

<div class="section">
	  <div class="container">
		<div class="row">
		  <div class="col-md-12">
			<div class="page-header">
			  <h1>Subir Resultados Evento </h1>
			  {{$evento->nombre}} - {{$evento->circuito->nombre}} <br />
			  {{$evento->fecha}} {{$evento->hora}}
			</div>
		  </div>
		</div>
	  </div>
	</div>
	<div class="section">
		<form role="form" method="POST" id="formEventos" action="{{ action('EventosController@storeLog')}}" enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="campeonato" value="{{ $campeonato }}">
		<input type="hidden" name="evento_id" value="{{ $evento->id }}">
	  <div class="container">
		<div class="row">
		  <div class="col-md-12">
			
			<div class="col-md-7">
				<div class="form-group">
				  <label class="control-label" for="eventoLog">Subir Log*</label>
				  <input accept="*.xml" class="form-control required" id="eventoLog" name="eventoLog" placeholder="Log de resultados" type="file">


				</div>
				
				<button class="btn btn-lg btn-primary btn-block" type="submit">Grabar</button>

			</div>
		  </div>
		</div>
	  </div>
	  </form>
	</div>

@endsection