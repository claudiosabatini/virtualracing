<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Eventos;
use App\PuntuacionCampeonatos;

class Campeonatos extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'campeonatos';


	/**
     * Función que devuelve el administrador
     *
     */
    public function administrador(){
        return $this->belongsTo('App\Usuarios');
    }

    /**
     * Función que devuelve el mod
     *
     */
    public function mod(){
        return $this->belongsTo('App\Mods');
    }

    /**
     * Función que devuelve los eventos
     *
     */
    public function eventos(){
        return $this->hasMany('App\Eventos')->orderBy('order');
    }

    /**
     * Función que devuelve las puntuaciones
     *
     */
    public function puntuacionCampeonatos(){
        return $this->hasMany('App\PuntuacionCampeonatos');
    }

    public static function puntosPosicion($posicion,$id) {
        $puntos= PuntuacionCampeonatos::where('campeonatos_id',$id)->where('posicion',$posicion)->first();
        if($puntos==null) {
            $puntosPosicion = 0;
        } else {
            $puntosPosicion = $puntos->puntos;
        }
        return $puntosPosicion;
    }

    public function getPosicionesCampeonatoAttribute(){
        $pilotos = [];
        $eventos = Eventos::where('campeonatos_id',$this->id)->where('completado',true)->get();
        if(count($eventos)>0) {
            $puntuaciones = $this->puntuacionCampeonatos()->get();
            
            foreach($eventos as $evento) {
                foreach($evento->resultados as $resultado){
                    //dd($resultado->pilotos()->first());
                    if(array_key_exists($resultado->pilotos()->first()->nombre, $pilotos)){
                        $pilotos[$resultado->pilotos()->first()->nombre]['puntos']+= self::puntosPosicion($resultado->posicion,$this->id);
                    } else {
                        $pilotos[$resultado->pilotos()->first()->nombre]= [
                            'nombre'        => $resultado->pilotos()->first()->nombre,
                            'escuderia'     => @$resultado->pilotos()->first()->pilotos_escuderias->nombre,
                            'puntos'        => $this->puntosPosicion($resultado->posicion,$this->id)
                            ];
                    }
                        
                }
            }
            //$pilotos['claudiosan']=['nombre'=>'Claudiosan','escuderia'=>'Escuderia','puntos'=>42];

            usort($pilotos,array("\App\Http\Controllers\Controller","ordenarPilotosPorPuntos"));
        }

        return $pilotos;

    }

}
