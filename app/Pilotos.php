<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\PilotosEscuderias;
use App\Escuderias;

class Pilotos extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'pilotos';


	/**
     * Función que devuelve el usuario
     *
     */
    public function usuario(){
        return $this->belongsTo('App\Usuarios');
    }

    /**
     * Funcion que devuelve los usuarios escuderia
     */
    public function pilotosEscuderias(){
    	return $this->hasMany('App\PilotosEscuderias');
    }

    /**
     * Funcion que devuelve la escuderia
     */
    public function getPilotosEscuderiasAttribute(){
        $pilotos_escuderias = PilotosEscuderias::where('pilotos_id',$this->id)->first();
        return @Escuderias::find($pilotos_escuderias->escuderias_id);
    }

    /**
     * Funcion que devuelve los resultados
     */
    public function resultados(){
    	return $this->hasMany('App\Resultados');
    }

}
