<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Paises extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'paises';


	/**
     * Función que devuelve las marcas asociados al pais
     *
     */
    public function marcas(){
        return $this->hasMany('App\Marcas');
    }

}
