<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Paises;

class Circuitos extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'circuitos';


	/**
     * Función que devuelve el pais
     *
     */
    public function pais(){
        return $this->belongsTo('App\Paises');
    }

    public function getLocalizacionAttribute() {
        return Paises::where('pais',$this->pais)->first();
    }

    /**
     * Función que devuelve los eventos
     *
     */
    public function eventos(){
        return $this->hasMany('App\Eventos');
    }

}
