<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Vueltas extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'vueltas';


	/**
     * Funcion que devuelve el resultado
     */
    public function resultado(){
    	return $this->belongsTo('App\Resultados');
    }

}
