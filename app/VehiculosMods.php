<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class VehiculosMods extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'vehiculos_mods';


	/**
     * Función que devuelve el mod
     *
     */
    public function mod(){
        return $this->belongsTo('App\Mods');
    }

    /**
     * Función que devuelve la vehiculo
     *
     */
    public function vehiculo(){
        return $this->belongsTo('App\Vehiculos');
    }

}
