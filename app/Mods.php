<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Mods extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'mods';

	/**
	 * devuelve los vehiculos de un mod
	 */
	public function vehiculosMods() {
		return $this->hasMany('App\VehiculosMods');
	}

}
