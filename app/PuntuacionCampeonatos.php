<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PuntuacionCampeonatos extends Model {

	/**
     * Función que devuelve el campeonato
     *
     */
    public function campeonato(){
        return $this->belongsTo('App\Campeonatos');
    }

}
