<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PilotosEscuderias extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'pilotos_escuderias';


	/**
     * Función que devuelve el piloto
     *
     */
    public function piloto(){
        return $this->belongsTo('App\Pilotos');
    }

    /**
     * Función que devuelve la escuderia
     *
     */
    public function escuderia(){
        return $this->belongsTo('App\Escuderias');
    }

	/**
     * Función que devuelve los vehiculos
     *
     */
    public function vehiculos(){
        return $this->belongsToMany('App\Vehiculos');
    }

}
