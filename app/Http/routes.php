<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::post('cambiar-idioma',['uses'=>'GeneralController@changeLanguage']);

Route::get('/perfil/login',['uses'	=> 'Perfil\UserController@login']);
Route::group(['prefix' => 'perfil', 'middleware' => 'validado', 'namespace' => 'Perfil'], function(){

	Route::get('/try', 'WelcomeController@index');
	
});


Route::get('campeonatos',['uses'=>'CampeonatosController@index']);
Route::get('mis-campeonatos', ['middleware' => 'validado','uses'=>'CampeonatosController@mios']);
Route::get('campeonato/{id}/{nombre}',['uses'=>'CampeonatosController@show']);
Route::get('campeonato/{c_id}/{nombre}/resultados/',function($c_id,$nombre){
	return Redirect::to(url('campeonato/'.$c_id.'/'.$nombre));
});
Route::get('campeonato/{c_id}/{nombre}/resultados/{id}',['uses'=>'EventosController@show']);
Route::get('campeonato/{c_id}/{nombre}/subir-resultados/{id}',['uses'=>'EventosController@subir', 'middleware'=>'validado']);
Route::get('campeonato/{c_id}/{nombre}/agregar-evento',['uses'=>'EventosController@add', 'middleware'=>'validado']);


Route::get('circuitos/{id}/{nombre}',['uses'=>'CircuitosController@show']);
Route::get('circuitos',['uses'=>'CircuitosController@index']);
Route::get('circuitos/agregar',['uses'=>'CircuitosController@add', 'middleware'=>'validado']);



//POST
Route::post('circuitos/agregar',['uses'=>'CircuitosController@store', 'middleware'=>'validado']);
Route::post('eventos/agregar',['uses'=>'EventosController@store', 'middleware'=>'validado']);
Route::post('eventos/subir-log',['uses'=>'EventosController@storeLog', 'middleware'=>'validado']);