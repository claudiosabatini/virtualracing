<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Circuitos;
use App\Paises;
use App\Eventos;
use App\Campeonatos;
use App\Pilotos;
use App\Escuderias;
use App\PilotosEscuderias;
use App\Resultados;
use App\Vueltas;
use Validator;

use Illuminate\Http\Request;

class EventosController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	public function storeLog(Request $request){
		//dd($request);
		$file = \Request::file('eventoLog');
		if ($file!== null){
			//dd($file);
			if($file->getClientMimeType()=='text/xml'){
				$evento = Eventos::find($request->evento_id);
				$path = '/log/'.$request->campeonato.'/'.$evento->id.'/'.$file->getClientOriginalName();
				\Storage::put($path,  \File::get($file));
				$evento->log = $path;
				$evento->completado = 1;
				$evento->save();

				//$xml = simplexml_load_file(storage_path().'/app'.$path);
				$content = utf8_encode(file_get_contents(storage_path().'/app'.$path));
				$xml = simplexml_load_string($content);
				//dd($xml);
				foreach($xml->RaceResults->Race->Driver as $driver) {
					$piloto = Pilotos::where('nombre', $driver->Name)->first();
					if(!$piloto) {
						//echo "no hay piloto";
						$piloto = new Pilotos;
						$piloto->nombre = $driver->Name;
						$piloto->save();
					}
					$escuderia = Escuderias::where('nombre', $driver->TeamName)->first();
					if(!$escuderia) {
						$escuderia = new Escuderias;
						$escuderia->nombre = $driver->TeamName;
						$escuderia->save();
					}

					$resultado = new Resultados;
					$resultado->eventos_id = $evento->id;
					$resultado->posicion = $driver->Position;
					$resultado->pilotos_id = $piloto->id;
					$resultado->tiempo_total = $driver->FinishTime;
					$resultado->total_vueltas = $driver->Laps;
					$resultado->total_boxes = $driver->Pitstops;
					$resultado->estado = $driver->FinishStatus;

					$resultado->save();
					//dd($driver->Lap);
					//dd($driver->Lap->attributes());
					foreach($driver->Lap as $lap) {
						$vuelta = new Vueltas;
						$vuelta->resultados_id = $resultado->id;
						$vuelta->vuelta = $lap->attributes()['num'];
						$vuelta->tiempo_vuelta = $lap;
						$vuelta->parcial_1 = $lap->attributes()['s1'];
						$vuelta->parcial_2 = $lap->attributes()['s2'];
						$vuelta->parcial_3 = $lap->attributes()['s3'];
						$vuelta->boxes = (@$lap->attributes()['pit']==1)?1:0;
						$vuelta->penalizacion =0;
						$vuelta->gasolina = $lap->attributes()['fuel'];
						$vuelta->posicion = $lap->attributes()['p'];

						$vuelta->save();
					}
					//dd($driver);
				}
			}
		}
		$champ = Campeonatos::find($request->campeonato);
		return \Redirect::to('/campeonato/'.$request->campeonato.'/'.str_slug($champ->nombre,'-'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		//dd($request);
		if($request->circuito==""){
			$rules= ['circuitoNombre'=>'required','paisCircuito'=>'required','distanciaCircuito'=>'required|integer'];
			$fields=['circuitoNombre','paisCircuito','distanciaCircuito'];
			$this->modelValidator($request,$rules,$fields);

			//$pais = Paises::where('pais',$request->paisCircuito)->first();

			$circuito = new Circuitos;
			$circuito->nombre = $request->circuitoNombre;
			$circuito->pais = $request->paisCircuito;
			$circuito->longitud = $request->distanciaCircuito;
			$circuito->imagen = $request->urlImagenCircuito;
			$circuito->descripcion = $request->descripcionCircuito;
			
			\Session::flash('message','Circuito grabado');
			$circuito->save();
			

		} else {
			$circuito = Circuitos::find($request->circuito);
		}

		$rules= ['eventoNombre'=>'required',
			'eventoFecha'=>'required',
			'eventoHoras'=>'required|integer',
			'eventoMinutos' => 'required|integer',
			'eventoDuracion' => 'required|integer'
			];
		$fields=['eventoNombre','eventoFecha','eventoHoras','eventoMinutos','eventoDuracion'];
		$this->modelValidator($request,$rules,$fields); 

		$champ = Campeonatos::find($request->campeonato);
		$order = count($champ->eventos);

		$evento = new Eventos;
		$evento->campeonatos_id = $champ->id;
		$evento->order = ++$order;
		$evento->circuitos_id = $circuito->id;
		$evento->nombre = $request->eventoNombre;
		$evento->descripcion = $request->eventoDescripcion;
		$evento->logo = $request->eventoLogo;
		$evento->imagen = "";//$request->
		$evento->fecha = $request->eventoFecha;
		$evento->hora = $request->eventoHoras.":".$request->eventoMinutos;
		$evento->tipo = $request->eventoTipo;
		$evento->duracion = $request->eventoDuracion;

		$evento->save();
		\Session::flash('message','Evento grabado');

		return \Redirect::to('/campeonato/'.$request->campeonato.'/'.str_slug($champ->nombre,'-'));
		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($c_id,$nombre,$id)	{
		$campeonato = Campeonatos::find($id);
		if(!$campeonato) {
			return view('errors.404');
		}
		if(str_slug($campeonato->nombre)!=$nombre){
			return view('errors.404');
		}
		$evento = Eventos::find($id);
		$resultados = Resultados::where('eventos_id',$id)->orderBy('posicion')->get();
		return view('eventos.mostrar')->with(['campeonato'=>$campeonato,'posiciones'=>0,'evento'=>$evento,'resultados'=>$resultados]);
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function add($c_id,$nombre){
		$circuitos = Circuitos::all();
		$paises = Paises::all();

		return view('eventos.agregar')->with(['circuitos'=>$circuitos,'paises'=>$paises,'campeonato'=>$c_id]);
	}

	public function subir($c_id,$nombre,$id){
		$evento = Eventos::find($id);
		return view('eventos.subir-resultados')->with(['evento'=>$evento,'nombre'=>$nombre,'campeonato'=>$c_id]);
	}

}
