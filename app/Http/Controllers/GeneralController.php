<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class GeneralController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	public function changeLanguage(Request $request) {
        
        if (array_key_exists($request->language, \Config::get('languages'))) {
            \Session::set('applocale', $request->language);
            return true;
        }
        return false;
    }

}
