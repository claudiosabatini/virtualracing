<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Campeonatos;

use Illuminate\Http\Request;

class CampeonatosController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$campeonatos = Campeonatos::where('public',1)->get();
		return view('campeonatos.listado')->with(['campeonatos'=>$campeonatos]);
		
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function mios()
	{
		$campeonatos = Campeonatos::where('administrador',\Auth::user()->id)->get();
		return view('campeonatos.listado')->with(['campeonatos'=>$campeonatos]);
		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id,$name)
	{
		$campeonato = Campeonatos::find($id);
		if(!$campeonato) {
			return view('errors.404');
		}
		if(str_slug($campeonato->nombre)!=$name){
			return view('errors.404');
		}
		return view('campeonatos.mostrar')->with(['campeonato'=>$campeonato,'posiciones'=>0]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
