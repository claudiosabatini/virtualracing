<?php namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

abstract class Controller extends BaseController {

	use DispatchesCommands, ValidatesRequests;

	public static function ordenarPilotosPorPuntos($piloto1,$piloto2) {
        if($piloto1['puntos']==$piloto2['puntos']){
            return 0;
        }

        return ($piloto1['puntos']<$piloto2['puntos'])?1:-1;
    } 

    /**
    * Valida los campos recibidos por los formularios. En $model_rules le pasamos un array con las reglas de validaciones que necesitamos ejecutar y en $fields un array con los campos a validar
    * @param Request $resquest required
    * @param $model_rules (array with model rules to validate)
    * @param $fields array with fields name to validate (all by default)
    * @author Claudio
    * @version 1
    *
    * @return void or fails if not validate
    */

    public function modelValidator($request, $model_rules, $fields="all") { //el nombre a usar bien deberia ser este
        $rules= $this->getModelValidatorRules($model_rules,$fields);
        $this->validate($request, $rules);
    }

    /**
    * Devuelve un array ordenado como los fields de las reglas
    * @param $rules array con reglas clave=>valor
    * @param $fields array con campos
    * @author Claudio
    * @version 1
    *
    * @return array - clave=>valor
    */
    public function getModelValidatorRules($rules,$fields) {
        if($fields=="all") {
            return $rules;
        }
        $all_rules = $rules;
        $rules_keys = array_keys($rules);
        $rules_ = [];
        foreach($fields as $data_) {
            if(in_array($data_,$rules_keys)){
                //$key = $rules[$data_];
                //dd($data_);
                $rules_ = array_merge($rules_,[$data_=>$rules[$data_]]);
            }
        }   
        if($rules_==[]) { //si no existe ninguna regla, las vuelve a asignar a todas.
            $rules_ = $all_rules;
        } 
        return $rules_;
    }

}
