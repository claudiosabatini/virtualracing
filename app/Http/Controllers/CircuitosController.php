<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Paises;
use App\Circuitos;

use Illuminate\Http\Request;

class CircuitosController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$circuitos = Circuitos::all();
		return view('circuitos.listado')->with(['circuitos'=>$circuitos]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$rules= ['circuitoNombre'=>'required','paisCircuito'=>'required','distanciaCircuito'=>'required|integer'];
		$fields=['circuitoNombre','paisCircuito','distanciaCircuito'];
		$this->modelValidator($request,$rules,$fields);

		//$pais = Paises::where('pais',$request->paisCircuito)->first();

		$circuito = new Circuitos;
		$circuito->nombre = $request->circuitoNombre;
		$circuito->pais = $request->paisCircuito;
		$circuito->longitud = $request->distanciaCircuito;
		$circuito->imagen = $request->urlImagenCircuito;
		$circuito->descripcion = $request->descripcionCircuito;
		
		\Session::flash('message','Circuito grabado');
		$circuito->save();

		return \Redirect::to('/');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id,$nombre)
	{
		$circuito = Circuitos::find($id);
		return view('circuitos.mostrar')->with(['circuito'=>$circuito]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function add(){
		$paises = Paises::all();
		return view('circuitos.agregar')->with(['paises'=>$paises]);
	}

}
