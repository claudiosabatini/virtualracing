<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Escuderias extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'escuderias';

    /**
     * Funcion que devuelve el pais
     */
    public function pais() {
    	return $this->belongsTo('App\Paises');
    }

    /**
     * Funcion que devuelve los usuarios escuderia
     */
    public function pilotosEscuderias(){
    	return $this->hasMany('App\PilotosEscuderias');
    }


}
