<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Pilotos;
use App\Vueltas;
use App\Eventos;
use App\Campeonatos;

class Resultados extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'resultados';


	/**
     * Función que devuelve el evento
     *
     */
    public function evento(){
        return $this->belongsTo('App\Eventos');
    }

    /**
     * Función que devuelve el piloto
     *
     */
    public function pilotos(){
        return $this->belongsTo('App\Pilotos');
    }

    public function getPilotoAttribute(){
        return Pilotos::find($this->pilotos_id);
    }

    public function getTiempoCarreraAttribute(){
        $tiempo = $this->tiempo_total;
        $minutos = intval($tiempo/60);
        $resta_tiempo = $tiempo-($minutos*60);
        $resta_tiempo = explode(".", $resta_tiempo);
        return $minutos.":".sprintf("%02d.%03d",$resta_tiempo[0],@$resta_tiempo[1]);
    }

    public function getTiempoVueltaRapidaAttribute(){
        $vueltas = Vueltas::where('resultados_id',$this->id)->orderBy('tiempo_vuelta')->first();
        if(!$vueltas) {
            return "-:--.---";
        }
        $tiempo = $vueltas->tiempo_vuelta;
        $minutos = intval($tiempo/60);
        $resta_tiempo = $tiempo-($minutos*60);
        $resta_tiempo = explode(".", $resta_tiempo);
        return $minutos.":".sprintf("%02d.%03d",$resta_tiempo[0],@$resta_tiempo[1]);
    }

    public function getPuntosAttribute() {
        $evento = Eventos::find($this->eventos_id);
        $campeonato = Campeonatos::find($evento->campeonatos_id);
        return Campeonatos::puntosPosicion($this->posicion,$campeonato->id);
    }

    /**
     * Funcion que devuelve los vueltas
     */
    public function vueltas(){
    	return $this->hasMany('App\Vueltas');
    }

}
