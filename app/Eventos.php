<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Eventos extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'eventos';


	/**
     * Función que devuelve el campeonato
     *
     */
    public function campeonato(){
        return $this->belongsTo('App\Campeonatos');
    }

    /**
     * Función que devuelve el circuito
     *
     */
    public function circuito(){
        return $this->belongsTo('App\Circuitos');
    }

    public function getCircuitoAttribute(){
        return Circuitos::find($this->circuitos_id);
    }

    /**
     * Funcion que devuelve los resultados
     */
    public function resultados(){
    	return $this->hasMany('App\Resultados');
    }

}
