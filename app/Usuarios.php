<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Usuarios extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'usuarios';

	public $user_type = ['admin','editor','normal'];


	/**
     * Función que devuelve los pilotos asociados al usuario
     *
     */
    public function pilotos(){
        return $this->hasMany('App\Pilotos');
    }

    /**
     * Funcion que devuelve el pais
     */
    public function pais() {
    	return $this->belongsTo('App\Paises');
    }

}
