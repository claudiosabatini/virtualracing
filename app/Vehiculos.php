<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehiculos extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'vehiculos';

	public $tipo = ['camion',
				'camioneta', //pick-up
				'crossover',
				'deportivo', //coupe
				'electrico',
				'formula',
				'gt',
				'monovolumen',
				'prototipo',
				'superdeportivo',
				'turismo',
				'utilitario'];

	public $tipo_motor = ['diesel',
				'electrico',
				'hibrido',
				'gasolina'];

	public $traccion = ['delantera',
				'integral',
				'trasera'];


	/**
     * Función que devuelve marca
     *
     */
    public function marca(){
        return $this->hasMany('App\Marcas');
    }

}
