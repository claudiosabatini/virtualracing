<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Marcas extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'marcas';


	/**
     * Función que devuelve los vehiculos asociados a la marca
     *
     */
    public function vehiculos(){
        return $this->hasMany('App\Vehiculos');
    }

}
